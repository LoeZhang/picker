package com.loe.test

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.bigkoo.pickerview.TimePickerView
import com.loe.picker.*
import com.loe.picker.util.OnLoadOkListener
import com.loe.picker.util.PickerBean
import com.loe.picker.util.SameEngine
import com.loe.picker.util.XTime
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.text = XTime().addDate(-800).formatDynamic2()

        val pickerDialog = PickerDialog(
            this, listOf(
                PickerBean("哈哈"),
                PickerBean("多少"),
                PickerBean("断点顺丰到付"),
                PickerBean("434"),
                PickerBean("同仁堂"),
                PickerBean("灌灌灌灌付拖"),
                PickerBean("sd"),
                PickerBean("欧体"),
                PickerBean("谷")
            )
        )
        { i, bean ->
            button.text = bean.name
        }

        var i = 0

        val pickerNetDialog = PickerNetDialog(this, object : PickerNetDialog.PickerCallback
        {
            override fun onSelect(index: Int, bean: PickerBean)
            {
                button.text = bean.name
            }

            override fun onLoadList(onLoadOkListener: OnLoadOkListener)
            {
                Handler().postDelayed({

                    if (i == 0)
                    {
                        i++
                        onLoadOkListener.onLoadOk(null);
                        return@postDelayed
                    }
                    onLoadOkListener.onLoadOk(
                        listOf(
                            PickerBean("哈哈"),
                            PickerBean("多少"),
                            PickerBean("断点顺丰到付"),
                            PickerBean("434"),
                            PickerBean("同仁堂"),
                            PickerBean("灌灌灌灌付拖"),
                            PickerBean("sd"),
                            PickerBean("欧体"),
                            PickerBean("谷")
                        )
                    )

                }, 2000)

            }
        })

        var pickerDoubleDialog = PickerDoubleDialog(this, listOf(
            PickerBean("哈哈"),
            PickerBean("多少"),
            PickerBean("断点顺丰到付"),
            PickerBean("434"),
            PickerBean("同仁堂"),
            PickerBean("灌灌灌灌付拖"),
            PickerBean("sd"),
            PickerBean("欧体"),
            PickerBean("谷")
        ), object : PickerDoubleDialog.PickerCallback
        {
            override fun onSelect(index1: Int, index2: Int, bean1: PickerBean, bean2: PickerBean): Boolean
            {
                button.text = bean2.name
                return true
            }

            override fun onLoadList2(pi1: Int, parent: PickerBean, onLoadOkListener: OnLoadOkListener)
            {
                onLoadOkListener.onLoadOk(
                    listOf(
                        PickerBean(parent.name + "4343"),
                        PickerBean(parent.name + "多少"),
                        PickerBean(parent.name + "谷歌"),
                        PickerBean(parent.name + "辅导辅导辅导"),
                        PickerBean(parent.name + "忐忑热热热"),
                        PickerBean(parent.name + "灌灌付"),
                        PickerBean(parent.name + "退入"),
                        PickerBean(parent.name + "他"),
                        PickerBean(parent.name + "让54")
                    )
                )
            }
        })

        var pickerNetDoubleDialog = PickerNetDoubleDialog(this, object : PickerNetDoubleDialog.PickerCallback
        {
            override fun onSelect(index1: Int, index2: Int, bean1: PickerBean, bean2: PickerBean): Boolean
            {
                button.text = bean2.name
                return true
            }

            override fun onLoadList1(onLoadOkListener: OnLoadOkListener)
            {
                Handler().postDelayed({

                    if (i == 0)
                    {
                        i++
                        onLoadOkListener.onLoadOk(null);
                        return@postDelayed
                    }
                    onLoadOkListener.onLoadOk(
                        listOf(
                            PickerBean("哈哈"),
                            PickerBean("多少"),
                            PickerBean("断点顺丰到付"),
                            PickerBean("434"),
                            PickerBean("同仁堂"),
                            PickerBean("灌灌灌灌付拖"),
                            PickerBean("sd"),
                            PickerBean("欧体"),
                            PickerBean("谷")
                        )
                    )

                }, 2000)

            }

            override fun onLoadList2(pi1: Int, parent: PickerBean, onLoadOkListener: OnLoadOkListener)
            {
                onLoadOkListener.onLoadOk(
                    listOf(
                        PickerBean(parent.name + "4343"),
                        PickerBean(parent.name + "多少"),
                        PickerBean(parent.name + "谷歌"),
                        PickerBean(parent.name + "辅导辅导辅导"),
                        PickerBean(parent.name + "忐忑热热热"),
                        PickerBean(parent.name + "灌灌付"),
                        PickerBean(parent.name + "退入"),
                        PickerBean(parent.name + "他"),
                        PickerBean(parent.name + "让54")
                    )
                )
            }
        })


        var pickerNetThreeDialog = PickerNetThreeDialog(this, object : PickerNetThreeDialog.PickerCallback
        {
            override fun onSelect(index1: Int, index2: Int, index3: Int, bean1: PickerBean, bean2: PickerBean, bean3: PickerBean): Boolean
            {
                button.text = bean3.name
                return true
            }

            override fun onLoadList1(onLoadOkListener: OnLoadOkListener)
            {
                Handler().postDelayed({

                    if (i == 0)
                    {
                        i++
                        onLoadOkListener.onLoadOk(null);
                        return@postDelayed
                    }
                    onLoadOkListener.onLoadOk(
                        listOf(
                            PickerBean("哈哈"),
                            PickerBean("多少"),
                            PickerBean("断点顺丰到付"),
                            PickerBean("434"),
                            PickerBean("同仁堂"),
                            PickerBean("灌灌灌灌付拖"),
                            PickerBean("sd"),
                            PickerBean("欧体"),
                            PickerBean("谷")
                        )
                    )

                }, 2000)
            }

            override fun onLoadList2(pi1: Int, parent: PickerBean, onLoadOkListener: OnLoadOkListener)
            {
                onLoadOkListener.onLoadOk(
                    listOf(
                        PickerBean(parent.name + "4343"),
                        PickerBean(parent.name + "多少"),
                        PickerBean(parent.name + "谷歌"),
                        PickerBean(parent.name + "辅导辅导辅导"),
                        PickerBean(parent.name + "忐忑热热热"),
                        PickerBean(parent.name + "灌灌付"),
                        PickerBean(parent.name + "退入"),
                        PickerBean(parent.name + "他"),
                        PickerBean(parent.name + "让54")
                    )
                )
            }

            override fun onLoadList3(pi1: Int, pi2: Int, parent: PickerBean, onLoadOkListener: OnLoadOkListener)
            {
                onLoadOkListener.onLoadOk(
                    listOf(
                        PickerBean(parent.name + "1"),
                        PickerBean(parent.name + "2"),
                        PickerBean(parent.name + "3"),
                        PickerBean(parent.name + "4"),
                        PickerBean(parent.name + "5"),
                        PickerBean(parent.name + "6"),
                        PickerBean(parent.name + "7"),
                        PickerBean(parent.name + "8"),
                        PickerBean(parent.name + "9")
                    )
                )
            }
        })
        pickerNetThreeDialog.setTail("申", "全", "元")

        val pickerTimeDialog = PickerTimeDialog(this, PickerTimeDialog.Type.DATE_TIME, null, XTime().fullDay())
        {
            button.text = it.formatStaticDate() + "   距离今天 " + it.toNowDate()
        }


        val builder = TimePickerView.Builder(this)
        { date, v ->
            Toast.makeText(this, date.toString(), Toast.LENGTH_SHORT).show()
        }
            .setContentSize(16)
            .setLabel("", "", "", "", "", "")
            .setTextColorCenter(getResources().getColor(R.color.colorPrimary))
            .setTextColorOut(-0x444445)
            .setDecorView(frameView)

        val pickerView = builder.build()

        button.setOnClickListener()
        {
//            picker.selectSameName("付出")
//            picker.show()
//            pickerNetDialog.show()
//            pickerNetThreeDialog.show()

//            pickerTimeDialog.show()
//            pickerView.show()

            pickerNetThreeDialog.show()
        }


        val sameEngine = SameEngine("135")
        sameEngine.add("12345")
        sameEngine.add("21356")
        sameEngine.add("1")
        sameEngine.add("33")
        sameEngine.add("15")
        sameEngine.add("648")
        sameEngine.add("53")
        sameEngine.add("876355")
        sameEngine.add("9987676")
        sameEngine.add("76")
        sameEngine.add("0")
        sameEngine.add("13")
        sameEngine.add("543")
        sameEngine.add("8")
        sameEngine.add("455454")
        sameEngine.add("0015551313")
        textView.text = sameEngine.getOrderList(33f).joinToString("\n") { it.name + " - " + it.sameRate }
    }
}
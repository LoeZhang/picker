package com.loe.picker.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class PickerBean
{
    private String code;
    private String name;
    private JSONObject data;
    private boolean isSelect = false;

    public static String CODE_KEY = "value";
    public static String NAME_KEY = "text";

    public static ArrayList<PickerBean> toList(JSONArray js)
    {
        ArrayList<PickerBean> list = new ArrayList<>();
        for (int i = 0; i < js.length(); i++)
        {
            JSONObject json = js.optJSONObject(i);
            list.add(new PickerBean(json));
        }
        return list;
    }

    public PickerBean(String code)
    {
        setCode(code);
        setName(code);
    }

    public PickerBean(String code, String name)
    {
        setCode(code);
        setName(name);
    }

    public PickerBean(String code, String name, JSONObject data)
    {
        setCode(code);
        setName(name);
        setData(data);
    }

    public PickerBean(JSONObject json)
    {
        try
        {
            code = json.optString(CODE_KEY);
            name = json.optString(NAME_KEY);

            if (code == null || "".equals(code))
            {
                code = name;
            }
            if (name == null || "".equals(name))
            {
                name = code;
            }
            data = json;
        } catch (Exception e)
        {
            Log.e("PickerBean", e.toString());
        }
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isSelect()
    {
        return isSelect;
    }

    public void setSelect(boolean select)
    {
        isSelect = select;
    }

    public JSONObject getData()
    {
        return data;
    }

    public void setData(JSONObject data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return name;
    }

}
